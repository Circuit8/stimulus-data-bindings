import resolve from '@rollup/plugin-node-resolve'
import { terser } from 'rollup-plugin-terser'
import pkg from './package.json'

const terserOptions = {
  mangle: false,
  compress: false,
  format: {
    beautify: true,
    indent_level: 2
  }
}

export default [
  {
    input: 'src/index.js',
    output: [
      {
        file: pkg.main,
        format: 'umd',
        name: 'DataBindingController'
      },

      {
        file: pkg.module,
        format: 'es'
      }
    ],
    plugins: [
      resolve(),
      terser(terserOptions)
    ]
  }
]
