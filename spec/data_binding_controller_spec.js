import {
  registerApplication,
  findTestInput,
  findTestOutput,
  nextTick,
  triggerChange
} from './helpers'
import DataBindingController from '../dist/stimulus-data-bindings.esm.js'
import chai, { expect } from 'chai'
import chaiDom from 'chai-dom'
import sinonChai from 'sinon-chai'

chai.use(chaiDom)
chai.use(sinonChai)
const consoleSpy = sinon.spy(console, 'log');

before(function () {
  registerApplication.call(this, 'data-binding', DataBindingController)
  this.sandbox = sinon.createSandbox()
})

afterEach(function () {
  this.sandbox.restore()
})

describe('without a condition', () => {
  describe('when value is fixed', () => {
    it('should set the value onto textContent', async () => {
      await nextTick()
      expect(findTestOutput('test1')).to.have.text('never changes')
    })
  })

  describe('when value is $value', () => {
    it('should always mirror value onto the textContent', async () => {
      expect(findTestOutput('test2')).not.to.have.text('text changed')
      await triggerChange(findTestInput('test2'), 'value', 'text changed')
      expect(findTestOutput('test2')).to.have.text('text changed')
    })

    it('should always mirror value onto a data field', async () => {
      expect(findTestOutput('test3')).to.have.attribute('data-foo', '')
      await triggerChange(findTestInput('test3'), 'value', 'new data')
      expect(findTestOutput('test3')).to.have.attribute('data-foo', 'new data')
    })
  })

  describe('with initial set to false', () => {
    it('should not load the value initially', async () => {
      await nextTick()
      expect(findTestOutput('test12')).not.to.have.text('badger')
    })
  })

  describe('with initial set to true', () => {
    it('should load the value initially', async () => {
      await nextTick()
      expect(findTestOutput('test13')).to.have.text('badger')
    })
  })
})

describe('with a condition', () => {
  describe('with a single target', () => {
    describe('with a text field', () => {
      it('should work when a basic equality condition passes', async () => {
        await triggerChange(findTestInput('test4'), 'value', 'does')
        expect(findTestOutput('test4')).to.have.attribute('foo', 'does')
      })

      it('should not update when the condition fails', async () => {
        await triggerChange(findTestInput('test4'), 'value', 'doesnt')
        expect(findTestOutput('test4')).to.not.have.attribute('foo', '')
      })

      it('should work with not equal to and value evaluation', async () => {
        await triggerChange(findTestInput('test5'), 'value', 'does')
        expect(findTestOutput('test5')).to.have.text('edited-does')
      })

      it('should work for hiding something if the field is blank', async () => {
        expect(findTestOutput('test6')).to.be.displayed
        await triggerChange(findTestInput('test6'), 'value', '')
        expect(findTestOutput('test6')).to.not.be.displayed
      })
    })

    describe('with a number field', () => {
      it('should work with greater than', async () => {
        expect(findTestOutput('test7')).to.be.displayed
        await triggerChange(findTestInput('test7'), 'value', 21)
        expect(findTestOutput('test7')).to.not.be.displayed
      })

      it('should work with less than', async () => {
        expect(findTestOutput('test8')).to.be.displayed
        await triggerChange(findTestInput('test8'), 'value', 4)
        expect(findTestOutput('test8')).to.not.be.displayed
      })
    })

    describe('with a checkbox field', () => {
      it('should hide and disable something if the field is not checked', async () => {
        expect(findTestOutput('test9')).to.be.displayed
        expect(findTestOutput('test9')).to.not.have.attribute('disabled')
        await triggerChange(findTestInput('test9'), 'checked', false)
        expect(findTestOutput('test9')).to.not.be.displayed
        expect(findTestOutput('test9')).to.have.attribute('disabled')
      })
    })

    describe('with class set', () => {
      it('should add a class when the condition passes', async () => {
        expect(findTestOutput('test15')).to.not.have.class('activated')
        await triggerChange(findTestInput('test15'), 'checked', true)
        expect(findTestOutput('test15')).to.have.class('activated')
      });

      it('should remove a class when the condition fails', async () => {
        expect(findTestOutput('test16')).to.have.class('activated')
        await triggerChange(findTestInput('test16'), 'checked', false)
        expect(findTestOutput('test16')).to.not.have.class('activated')
      });
    });
  })

  describe('with multiple targets', () => {
    it('should disable targets based on a select and their data', async () => {
      const id1 = fixture.el.querySelector(`#test10 [data-id="1"]`)
      const id2 = fixture.el.querySelector(`#test10 [data-id="2"]`)

      expect(id1).to.be.displayed
      expect(id2).to.not.be.displayed
      await triggerChange(findTestInput('test10'), 'value', '2')

      expect(id1).to.not.be.displayed
      expect(id2).to.be.displayed
    })

    it('should use the targets attributes but fallback to the parents', async () => {
      const output1 = fixture.el.querySelector(`#test11-1`)
      const output2 = fixture.el.querySelector(`#test11-2`)
      const output3 = fixture.el.querySelector(`#test11-3`)

      const input = findTestInput('test11')

      await triggerChange(findTestInput('test11'), 'value', '2')
      expect(output1).not.to.have.text('one')
      expect(output2).to.have.text('two')
      expect(output3).not.to.have.text('three')
    })

    it('should dispatch an event when the value is updated', async () => {
      const changeSpy = sinon.spy()
      const output = findTestOutput('test14')
      output.addEventListener('change', changeSpy)

      await triggerChange(findTestInput('test14'), 'value', 'foo')
      const call = changeSpy.getCall(0)
      expect(call.args[0].type).to.equal('change')
    })

    it('should console log debug info when debug is set', async () => {
      await nextTick()
      sinon.assert.calledWith(consoleSpy, sinon.match("stimulus-data-binding"))
      sinon.assert.calledWith(consoleSpy, sinon.match("No source elements found"))
    })
  })
})
