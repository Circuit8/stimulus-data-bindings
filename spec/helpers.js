import { Application } from 'stimulus'

export function registerApplication(id, controllerClass) {
  fixture.load('index.html')
  this._stimulusApp = Application.start()
  this._stimulusApp.register(id, controllerClass)
  this.controller = this._stimulusApp.controllers[0]
}

export function findTestInput(name) {
  return fixture.el.querySelector(`#${name}`)
}

export function findTestOutput(name) {
  return fixture.el.querySelector(`[data-binding-ref="${name}"]`)
}

export function nextTick() {
  return new Promise((resolve) => setTimeout(resolve, 0))
}

export function triggerChange(el, prop, value) {
  el[prop] = value
  el.dispatchEvent(new Event('change'))
  return nextTick()
}
